package hu.kniznertamas.compositedemo;

import java.util.Arrays;
import java.util.List;

import hu.kniznertamas.compositedemo.domain.AgeCategory;
import hu.kniznertamas.compositedemo.provider.AdultAgeCategoryProvider;
import hu.kniznertamas.compositedemo.provider.AgeCategoryProvider;
import hu.kniznertamas.compositedemo.provider.ChildAgeCategoryProvider;
import hu.kniznertamas.compositedemo.provider.CompositeAgeCategoryProvider;
import hu.kniznertamas.compositedemo.provider.OldAgeCategoryProvider;
import hu.kniznertamas.compositedemo.provider.YoungAgeCategoryProvider;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {
        CompositeAgeCategoryProvider compositeAgeCategoryProvider = new CompositeAgeCategoryProvider(createProviderList());

        int childAge = 5;
        AgeCategory child = compositeAgeCategoryProvider.getAdultStatus(childAge);
        System.out.println("Should be CHILD: " + child.toString());

        int youngAge = 16;
        AgeCategory young = compositeAgeCategoryProvider.getAdultStatus(youngAge);
        System.out.println("Should be YOUNG: " + young.toString());

        int adultAge = 42;
        AgeCategory adult = compositeAgeCategoryProvider.getAdultStatus(adultAge);
        System.out.println("Should be ADULT: " + adult.toString());

        int oldAge = 88;
        AgeCategory old = compositeAgeCategoryProvider.getAdultStatus(oldAge);
        System.out.println("Should be OLD: " + old.toString());
    }

    private List<AgeCategoryProvider> createProviderList() {
        return Arrays.asList(new ChildAgeCategoryProvider(), new YoungAgeCategoryProvider(), new AdultAgeCategoryProvider(), new OldAgeCategoryProvider());
    }
}
