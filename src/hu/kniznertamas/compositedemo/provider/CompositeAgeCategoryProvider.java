package hu.kniznertamas.compositedemo.provider;

import java.util.List;

import hu.kniznertamas.compositedemo.domain.AgeCategory;

/**
 * Composite {@link AgeCategoryProvider} implementation.
 *
 * @author Tamas_Knizner
 */
public class CompositeAgeCategoryProvider implements AgeCategoryProvider {

    private final List<AgeCategoryProvider> providers;

    public CompositeAgeCategoryProvider(final List<AgeCategoryProvider> providers) {
        this.providers = providers;
    }

    @Override
    public AgeCategory getAdultStatus(final int age) {
        AgeCategory ageCategory = null;

        // --------------------------------------------------------
        for (AgeCategoryProvider provider : providers) {
            ageCategory = provider.getAdultStatus(age);
            if (ageCategory != null) {
                break;
            }
        }

        // --------------------------------------------------------
//        for (int i = 0; i < providers.size() && ageCategory == null; i++) {
//            ageCategory = providers.get(i).getAdultStatus(age);
//        }

        // --------------------------------------------------------
//        ageCategory = providers.stream().map(provider -> provider.getAdultStatus(age)).filter(Objects::nonNull).findFirst().orElse(null);

        return ageCategory;
    }
}
