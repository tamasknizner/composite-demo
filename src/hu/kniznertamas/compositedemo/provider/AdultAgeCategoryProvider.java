package hu.kniznertamas.compositedemo.provider;

import hu.kniznertamas.compositedemo.domain.AgeCategory;

/**
 * Adult age category specific {@link AgeCategoryProvider} implementation.
 *
 * @author Tamas_Knizner
 */
public class AdultAgeCategoryProvider implements AgeCategoryProvider {
    @Override
    public AgeCategory getAdultStatus(final int age) {
        AgeCategory ageCategory = null;
        if (age >= 18 && age < 70) {
            ageCategory = AgeCategory.ADULT;
        }
        return ageCategory;
    }
}
