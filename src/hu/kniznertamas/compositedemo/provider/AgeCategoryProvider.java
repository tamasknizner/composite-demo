package hu.kniznertamas.compositedemo.provider;

import hu.kniznertamas.compositedemo.domain.AgeCategory;

/**
 * Provides an {@link AgeCategory} based on age input.
 *
 * @author Tamas_Knizner
 */
public interface AgeCategoryProvider {

    AgeCategory getAdultStatus(int age);

}
