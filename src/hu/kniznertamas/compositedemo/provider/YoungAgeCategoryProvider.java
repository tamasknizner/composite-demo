package hu.kniznertamas.compositedemo.provider;

import hu.kniznertamas.compositedemo.domain.AgeCategory;

/**
 * Young age category specific {@link AgeCategoryProvider} implementation.
 *
 * @author Tamas_Knizner
 */
public class YoungAgeCategoryProvider implements AgeCategoryProvider {
    @Override
    public AgeCategory getAdultStatus(final int age) {
        AgeCategory ageCategory = null;
        if (age >= 14 && age < 18) {
            ageCategory = AgeCategory.YOUNG;
        }
        return ageCategory;
    }
}
