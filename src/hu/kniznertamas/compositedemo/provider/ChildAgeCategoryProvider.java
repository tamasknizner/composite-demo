package hu.kniznertamas.compositedemo.provider;

import hu.kniznertamas.compositedemo.domain.AgeCategory;

/**
 * Child age category specific {@link AgeCategoryProvider} implementation.
 *
 * @author Tamas_Knizner
 */
public class ChildAgeCategoryProvider implements AgeCategoryProvider {
    @Override
    public AgeCategory getAdultStatus(final int age) {
        AgeCategory ageCategory = null;
        if (age < 14) {
            ageCategory = AgeCategory.CHILD;
        }
        return ageCategory;
    }
}
