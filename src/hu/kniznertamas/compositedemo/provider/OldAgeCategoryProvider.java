package hu.kniznertamas.compositedemo.provider;

import hu.kniznertamas.compositedemo.domain.AgeCategory;

/**
 * Old age category specific {@link AgeCategoryProvider} implementation.
 *
 * @author Tamas_Knizner
 */
public class OldAgeCategoryProvider implements AgeCategoryProvider {
    @Override
    public AgeCategory getAdultStatus(final int age) {
        AgeCategory ageCategory = null;
        if (age >= 70) {
            ageCategory = AgeCategory.OLD;
        }
        return ageCategory;
    }
}
