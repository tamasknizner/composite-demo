package hu.kniznertamas.compositedemo.domain;

/**
 * Enum to represent age categories.
 *
 * @author Tamas_Knizner
 */
public enum AgeCategory {
    CHILD,
    YOUNG,
    ADULT,
    OLD
}
